import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import React, { useState,useEffect } from 'react';

import Layout from '../components/Layout';
import { saveTask, getTask,updateTask } from '../api';

const TaskFormScreen = ({ navigation, route }) => {

  const [task, setTask] = useState({
    nombre: '',
    matricula: '',
    ci: '',
    telefono: '',
  });

  const [editing, setEditing] = useState(false);
  //console.log(route.params)
  const handleChange = (name, value) => setTask({ ...task, [name]: value });

  const handleSubmit = async () => {
    try{
      if (!editing){
        await saveTask(task);      
      }else{
        await updateTask(route.params.id, task);
      }
      navigation.navigate('HomeScreen');

    }catch (error){
      console.error(error);
    }
    
  };

  useEffect(() => {
    if(route.params && route.params.id){
      navigation.setOptions({headerTitle: 'Actualizar el registro'});
      setEditing(true);
    (async() => {
      const  task =  await getTask(route.params.id);
      setTask({nombre:task.nombre,matricula: task.matricula, ci: task.ci, fecha_nacimiento: task.fecha_nacimiento, telefono: task.telefono});
    })();
    }
  },[]);

  return (
    <Layout>
      <TextInput
        style={styles.input}
        placeholder='Nombre'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('nombre', text)}
        value={task.nombre}
      />
      <TextInput
        style={styles.input}
        placeholder='Matrícula'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('matricula', text)}
        value={task.matricula}
      />
       <TextInput
        style={styles.input}
        placeholder='C.I'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('ci', text)}
        value={task.ci}
      />
       <TextInput
        style={styles.input}
        placeholder='Teléfono'
        placeholderTextColor="#576574"
        onChangeText={(text) => handleChange('telefono', text)}
        value={task.telefono}
      />
      {
        !editing ?(
          <TouchableOpacity style={styles.buttonSave} onPress={handleSubmit}>
          <Text style={styles.buttonText}>Guardar</Text>
        </TouchableOpacity>  
        ):(
          <TouchableOpacity style={styles.buttonUpdate} onPress={handleSubmit}>
          <Text style={styles.buttonText}>Actualizar</Text>
        </TouchableOpacity>
        )}   
    </Layout>
  )
}

const styles = StyleSheet.create({
  input: {
    width: '90%',
    marginBottom: 7,
    fontSize: 14,
    borderWidth: 1,
    borderColor: '#10ac84',
    height: 35,
    color: '#ffffff',
    textAlign: 'center',
    borderRadius: 5,
    padding: 4
  },
  buttonSave: {
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 5,
    marginBottom: 10,
    backgroundColor: '#10ac84',
    width: '90%',
  },
  buttonText: {
    color: '#ffffff',
    textAlign: 'center'
  },
  buttonUpdate: {
    padding: 10,
    paddingBottom: 10,
    borderRadius: 5,
    marginBottom: 3,
    backgroundColor: "#e58e26",
    width: "90%"
  }
})

export default TaskFormScreen